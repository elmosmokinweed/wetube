import styles from './overflow-list.module.scss';

export const OverflowList: React.FC = ({ children }) => (
  <ul className={styles.root}>{children}</ul>
);
