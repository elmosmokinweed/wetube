import React from 'react';
import { Typography } from 'src/components/atoms/Typography';
import styles from './no-video.module.scss';

export type NoVideoProps = {
  text?: string;
};

export const NoVideo: React.FC<NoVideoProps> = ({
  text = 'Something went wrong :(',
}) => {
  return (
    <article className={styles.root}>
      <Typography
        className={styles.root__legend}
        color="paragraph"
        preset="title1"
        align="center"
      >
        {text}
      </Typography>
      <img
        className={styles.root__image}
        src="/images/404.png"
        width={500}
        height={500}
      ></img>
    </article>
  );
};
