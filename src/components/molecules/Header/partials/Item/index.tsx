import clsx from 'clsx';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { ReactElement } from 'react';
import { Typography } from 'src/components/atoms/Typography';
import styles from './item.module.scss';

export type ItemProps = {
  route: string;
  title: string;
  icon?: ReactElement;
};

export const Item: React.FC<ItemProps> = ({ route, title, icon }) => {
  const router = useRouter();
  return (
    <Link href={route}>
      <a
        className={clsx(styles.root, router.asPath === route && styles.current)}
      >
        <div className={styles.root__icon}>{icon}</div>
        <Typography
          color="paragraph"
          preset="common1"
          className={styles.root__title}
        >
          {title}
        </Typography>
      </a>
    </Link>
  );
};
