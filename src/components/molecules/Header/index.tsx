import clsx from 'clsx';
import Link from 'next/link';
import React, { useState } from 'react';
import { Typography } from 'src/components/atoms/Typography';
import { Bell } from 'src/icons/Bell';
import { Burger } from 'src/icons/Burger';
import { Download } from 'src/icons/Download';
import { Favorite } from 'src/icons/Favorite';
import { History } from 'src/icons/History';
import { Home } from 'src/icons/Home';
import { Like } from 'src/icons/Like';
import { Playlist } from 'src/icons/Playlist';
import { Profile } from 'src/icons/Profile';
import { WeTube } from 'src/icons/WeTube';
import styles from './header.module.scss';
import { Item } from './partials/Item';

const leftControls = [
  {
    title: 'Home',
    route: '/',
    icon: <Home />,
  },
  {
    title: 'Playlist',
    route: '/playlist',
    icon: <Playlist />,
  },
  {
    title: 'Favorite',
    route: '/favorite',
    icon: <Favorite />,
  },
  {
    title: 'History',
    route: '/history',
    icon: <History />,
  },
  {
    title: 'Download',
    route: '/download',
    icon: <Download />,
  },
  {
    title: 'Liked',
    route: '/liked',
    icon: <Like />,
  },
];

export const Header = () => {
  const [isBurgerToggled, setIsBurgerToggled] = useState(false);

  const toggleHandler = () => {
    setIsBurgerToggled(!isBurgerToggled);
  };

  return (
    <header className={styles.root}>
      <div className={styles.root__left}>
        <div className={styles.root__burger} onClick={() => toggleHandler()}>
          <Burger />
        </div>
        <Link href="/">
          <a className={styles.root__logo}>
            <WeTube />
            <Typography
              preset="logo"
              color="paragraph"
              className={styles.root__logo__text}
            >
              WeTube
            </Typography>
          </a>
        </Link>
      </div>
      <ul className={styles.root__right}>
        <li className={styles.root__mock__notification}>
          <Bell />
        </li>
        <li className={styles.root__mock__profile}>
          <Profile />
        </li>
      </ul>
      <nav
        className={clsx(
          styles.root__burger__menu,
          isBurgerToggled && styles.toggled
        )}
      >
        {leftControls.map(({ title, route, icon }) => {
          return <Item route={route} title={title} icon={icon} />;
        })}
      </nav>
      <div
        className={clsx(
          styles.root__burger__background,
          isBurgerToggled && styles.toggled
        )}
        onClick={() => setIsBurgerToggled(false)}
      />
    </header>
  );
};
