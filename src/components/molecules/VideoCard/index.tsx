import clsx from 'clsx';
import { useRouter } from 'next/router';
import React from 'react';
import { Typography } from 'src/components/atoms/Typography';
import { Video } from 'src/components/atoms/Video';
import styles from './video-card.module.scss';

export type VideoCardProps = {
  title: string;
  videoId: number;
  view?: 'side' | 'default';
  link: string;
  needPreview?: boolean;
};

export const VideoCard: React.FC<VideoCardProps> = ({
  title,
  videoId,
  link,
  view = 'default',
  needPreview,
}) => {
  const router = useRouter();
  const clickHandler = () => {
    router.push(`/watch/${videoId}`);
  };

  return (
    <div
      className={clsx(styles.root, styles[view])}
      onClick={() => clickHandler()}
    >
      <Video link={`${link}${needPreview && '#t=4'}`} />
      <Typography
        preset="description-card"
        color="paragraph"
        className={styles.root__title}
      >
        {title}
      </Typography>
    </div>
  );
};
