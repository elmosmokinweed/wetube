import clsx from 'clsx';
import { Color } from 'src/Types/Color';
import { Typography } from 'src/components/atoms/Typography';
import styles from './tag.module.scss';

type Theme = {
  hashtag: Color;
  tag: Color;
};

const THEME: Theme = {
  hashtag: 'accent',
  tag: 'paragraph',
};

export type TagProps = {
  mode: keyof Theme;
  state: 'selected' | 'disabled';
  className?: string;
  onClick: (id: number) => void;
  id: number;
};

export const Tag: React.FC<TagProps> = ({
  children,
  mode = 'tag',
  state,
  onClick,
  id,
  className: classNameFromProps,
}) => {
  const className = clsx(
    styles.root,
    styles[mode],
    styles[THEME[mode]],
    styles[state],
    classNameFromProps
  );

  return (
    <li className={className} onClick={() => onClick(id)}>
      <Typography preset="common2" color={THEME[mode]}>
        #{children}
      </Typography>
    </li>
  );
};
