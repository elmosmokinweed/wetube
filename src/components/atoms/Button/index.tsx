import React, { ReactElement } from 'react';
import styles from './button.module.scss';

export type ButtonProps = {
  text?: string;
  disabled?: boolean;
  icon?: ReactElement;
  onClick: () => void;
};

export const Button: React.FC<ButtonProps> = ({
  text,
  disabled,
  icon,
  onClick,
}) => {
  return (
    <button
      className={styles.root}
      onClick={() => onClick()}
      disabled={disabled}
    >
      {text && <p className={styles.root__text}>{text}</p>}
      {icon && <div className={styles.root__icon}>{icon}</div>}
    </button>
  );
};
