import clsx from 'clsx';
import React, { useRef } from 'react';
import styles from './video.module.scss';

export type VideoProps = {
  link: string;
  preview?: boolean;
  autoPlay?: boolean;
  onEnded?: () => void;
  width?: number;
  height?: number;
  className?: string;
  removePoster?: boolean;
};

export const Video: React.FC<VideoProps> = ({
  link,
  preview = true,
  autoPlay = false,
  onEnded,
  width,
  height,
  removePoster = false,
  className: classNameFromProps,
}) => {
  const videoRef = useRef<HTMLVideoElement | null>(null);

  const mouseOverHandler = (video: HTMLVideoElement) => {
    if (preview) {
      video.play();
    }
  };

  const onMouseOutHandler = (video: HTMLVideoElement) => {
    if (preview) {
      video.pause();
      video.currentTime = 4;
    }
  };

  const onLoaded = (video: HTMLVideoElement) => {
    if (removePoster) {
      video.removeAttribute('poster');
    }
  };

  return (
    <div className={clsx(styles.root, classNameFromProps)}>
      <video
        width={width}
        height={height}
        src={link}
        controls={!preview}
        className={styles.root__video}
        poster="/images/loading.gif"
        preload="auto"
        ref={videoRef}
        muted={preview}
        autoPlay={autoPlay}
        onLoadedData={(event) => {
          onLoaded(event.target as HTMLVideoElement);
        }}
        onMouseOver={(event) => {
          mouseOverHandler(event.target as HTMLVideoElement);
        }}
        onMouseOut={(event) => {
          onMouseOutHandler(event.target as HTMLVideoElement);
        }}
        onEnded={onEnded ? () => onEnded() : undefined}
      />
    </div>
  );
};
