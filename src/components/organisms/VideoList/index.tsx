import React from 'react';
import { VideoCard } from 'src/components/molecules/VideoCard';
import { VideoType } from 'src/Types/VideoType';
import styles from './video-list.module.scss';

export type VideoListProps = {
  content: VideoType[];
};

export const VideoList: React.FC<VideoListProps> = ({ content }) => {
  return (
    <section className={styles.root}>
      {content.map((video) => (
        <VideoCard
          key={video.id}
          link={video.src}
          title={video.title}
          videoId={video.id}
          needPreview
        />
      ))}
    </section>
  );
};
