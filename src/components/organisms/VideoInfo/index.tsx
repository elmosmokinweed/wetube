import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from 'src/components/atoms/Button';
import { Typography } from 'src/components/atoms/Typography';
import { IState } from 'src/core/store';
import { Favorite } from 'src/icons/Favorite';
import { PlaylsitAdd } from 'src/icons/PlaylistAdd';
import { PlaylistRemove } from 'src/icons/PlaylistRemove';
import { VideoType } from 'src/Types/VideoType';
import styles from './video-info.module.scss';

export type VideoInfoProps = {
  video: VideoType;
};

export const VideoInfo: React.FC<VideoInfoProps> = ({ video }) => {
  const currentVideo = useSelector((state: IState) => state.videoList);
  const currentFavorite = useSelector((state: IState) => state.favoriteList);
  const dispatch = useDispatch();

  const [isVideoDisabled, setIsVideoDisabled] = useState(
    currentVideo.map((video) => video.id).indexOf(video.id) !== -1
  );
  const [isFavoriteDisabled, setIsFavoriteDisabled] = useState(
    currentFavorite.map((video) => video.id).indexOf(video.id) !== -1
  );

  const videoClickHandler = () => {
    if (isVideoDisabled) {
      dispatch({ type: 'video/remove', payload: video.id });
      setIsVideoDisabled(false);
    } else {
      dispatch({ type: 'video/add', payload: video });
      setIsVideoDisabled(true);
    }
  };

  const favoriteClickHandler = () => {
    if (isFavoriteDisabled) {
      dispatch({ type: 'favorite/remove', payload: video.id });
      setIsFavoriteDisabled(false);
    } else {
      dispatch({ type: 'favorite/add', payload: video });
      setIsFavoriteDisabled(true);
    }
  };

  return (
    <section className={styles.root}>
      <Typography
        color="paragraph"
        preset="title1"
        className={styles.root__title}
      >
        {video.title}
      </Typography>
      <div className={styles.root__buttons}>
        <Button
          text={isVideoDisabled ? 'Remove from playlist' : 'Add to playlist'}
          onClick={videoClickHandler}
          icon={isVideoDisabled ? <PlaylistRemove /> : <PlaylsitAdd />}
        />
        <Button
          text={isFavoriteDisabled ? 'Remove from favorite' : 'Add to favorite'}
          onClick={favoriteClickHandler}
          icon={<Favorite />}
        />
      </div>
    </section>
  );
};
