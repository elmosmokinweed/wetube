import React from 'react';
import styles from './list.module.scss';

export type ListProps = {};

export const List: React.FC<ListProps> = ({ children }) => {
  return <ul className={styles.root}>{children}</ul>;
};
