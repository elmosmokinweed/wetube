import clsx from 'clsx';
import { useRouter } from 'next/router';
import React, { ReactElement } from 'react';
import { useDispatch } from 'react-redux';
import { Button } from 'src/components/atoms/Button';
import { Typography } from 'src/components/atoms/Typography';
import { Video } from 'src/components/atoms/Video';
import { OpenInNew } from 'src/icons/OpenInNew';
import { PlaylistRemove } from 'src/icons/PlaylistRemove';
import styles from './item.module.scss';

export type ItemProps = {
  title: string;
  onClick: () => void;
  src: string;
  id: number;
  isActive: boolean;
};

export type MiniButtonProps = {
  onClick: () => void;
  icon: ReactElement;
};

export const MiniButton: React.FC<MiniButtonProps> = ({ onClick, icon }) => {
  return (
    <button className={styles.root__button} onClick={onClick}>
      {icon}
    </button>
  );
};

export const Item: React.FC<ItemProps> = ({
  title,
  onClick,
  src,
  id,
  isActive = false,
}) => {
  const router = useRouter();

  const openInNewHandler = () => {
    router.push(`/watch/${id}`);
  };

  return (
    <li className={clsx(styles.root, isActive && styles.active)}>
      <div className={styles.root__controls}>
        <div className={styles.root__title} onClick={() => onClick()}>
          <Typography color="paragraph" preset="common1">
            {title}
          </Typography>
        </div>
        <div className={styles.root__buttons}>
          <MiniButton onClick={openInNewHandler} icon={<OpenInNew />} />
        </div>
      </div>
      <Video link={src} className={styles.root__video} removePoster />
    </li>
  );
};
