import React from 'react';
import { Video } from 'src/components/atoms/Video';
import { VideoType } from 'src/Types/VideoType';
import styles from './favorite-block.module.scss';
import { List } from './partials/List';
import { Item } from './partials/Item';
import { Typography } from 'src/components/atoms/Typography';
import { useDispatch } from 'react-redux';

export type FavoriteBlock = {
  index: number;
  videos: VideoType[];
  setVideo: (index: number) => void;
  onEnd: () => void;
};

export const FavoriteBlock: React.FC<FavoriteBlock> = ({
  index: currentVideoIndex,
  videos,
  setVideo,
  onEnd,
}) => {
  return (
    <section className={styles.root}>
      <div className={styles.root__video}>
        <Video
          link={videos[currentVideoIndex].src}
          preview={false}
          onEnded={onEnd}
          autoPlay
        />
        <Typography
          color="paragraph"
          preset="title1"
          className={styles.root__title}
        >
          {videos[currentVideoIndex].title}
        </Typography>
      </div>
      <div className={styles.root__playlist}>
        <List>
          {videos.map((video, index) => {
            return (
              <Item
                isActive={index === currentVideoIndex}
                key={video.id}
                onClick={() => setVideo(index)}
                title={video.title}
                src={video.src}
                id={video.id}
              />
            );
          })}
        </List>
      </div>
    </section>
  );
};
