import videos from 'src/mocks/videos.json';
import { VideoType } from 'src/Types/VideoType';

export default function handler(req: any, res: any) {
  const requestedId = parseInt(req.query.videoId);

  const requestedVideo = videos.content.find((video) => {
    return video.id === requestedId;
  });

  if (requestedVideo === undefined) {
    res.status(404).json('No video found');
  }

  res.status(200).json(requestedVideo);
}
