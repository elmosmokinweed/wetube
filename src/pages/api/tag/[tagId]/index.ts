import tags from 'src/mocks/tags.json';

export default function handler(req: any, res: any) {
  const requestedId = parseInt(req.query.tagId);

  const requestedTag = tags.content.find((tag) => {
    return tag.id === requestedId;
  });

  if (requestedTag === undefined) {
    res.status(404).json('No tag found');
  }

  res.status(200).json(requestedTag);
}
