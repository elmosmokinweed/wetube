import { NextPage } from 'next';
import Head from 'next/head';
import React from 'react';
import { Header } from 'src/components/molecules/Header';
import { NoVideo } from 'src/components/molecules/NoVideo';
import { Container } from 'src/components/templates/Container';

const Custom404: NextPage = () => {
  return (
    <>
      <Head>
        <title>Oops...</title>
      </Head>
      <Container>
        <Header />
        <NoVideo />
      </Container>
    </>
  );
};

export default Custom404;
