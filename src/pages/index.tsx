import axios from 'axios';
import type { NextPage } from 'next';
import Head from 'next/head';
import React, { useState } from 'react';
import { Header } from 'src/components/molecules/Header';
import { Container } from 'src/components/templates/Container';
import { VideoType } from 'src/Types/VideoType';
import { TagType } from 'src/Types/TagType';
import { VideoList } from 'src/components/organisms/VideoList';
import { OverflowList } from 'src/components/templates/OverflowList';
import { Tag } from 'src/components/atoms/Tag';

export type HomeProps = {
  videos: VideoType[];
  tags: TagType[];
};

const Home: NextPage<HomeProps> = ({ videos, tags }) => {
  const [selectedVideos, setSelectedVideos] = useState(videos);
  const [currentIndex, setCurrentIndex] = useState(0);

  const filterVideos = (id: number) => {
    setCurrentIndex(id);
    if (id === 0) {
      setSelectedVideos(videos);
    } else {
      const fitlered = videos.filter((video) => {
        return video.tagId.includes(id);
      });
      setSelectedVideos(fitlered);
    }
  };

  // console.log(videos[2].tagId.includes(1));

  return (
    <>
      <Head>
        <title>WeTube</title>
      </Head>
      <Header />
      <Container>
        <OverflowList>
          <Tag
            mode="tag"
            state={currentIndex === 0 ? 'selected' : 'disabled'}
            id={0}
            onClick={filterVideos}
          >
            all
          </Tag>
          {tags.map((tag) => {
            return (
              <Tag
                key={tag.id}
                mode="tag"
                state={currentIndex === tag.id ? 'selected' : 'disabled'}
                id={tag.id}
                onClick={filterVideos}
              >
                {tag.name}
              </Tag>
            );
          })}
        </OverflowList>
        <VideoList content={selectedVideos} />
      </Container>
    </>
  );
};

export const getServerSideProps = async () => {
  const videoData = await axios.get(`http://${process.env.API_URL}/api/video`);
  const tagData = await axios.get(`http://${process.env.API_URL}/api/tag`);

  const videos = videoData.data.content;
  const tags = tagData.data.content;

  return { props: { videos, tags } };
};

export default Home;
