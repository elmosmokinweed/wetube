import type { NextPage } from 'next';
import Head from 'next/head';
import React from 'react';
import { useSelector } from 'react-redux';
import { Typography } from 'src/components/atoms/Typography';
import { Header } from 'src/components/molecules/Header';
import { NoVideo } from 'src/components/molecules/NoVideo';
import { VideoList } from 'src/components/organisms/VideoList';
import { Container } from 'src/components/templates/Container';
import { IState } from 'src/core/store';

const Favorite: NextPage = () => {
  const currentFavorite = useSelector((state: IState) => state.favoriteList);

  return (
    <>
      <Head>
        <title>Favorite</title>
      </Head>
      <Header />
      <Container>
        {currentFavorite.length !== 0 ? (
          <>
            <Typography color="paragraph" preset="title1" align="center">
              Your favorite videos
            </Typography>
            <VideoList content={currentFavorite} />
          </>
        ) : (
          <NoVideo text="There is no video at your favorite :(" />
        )}
      </Container>
    </>
  );
};

export default Favorite;
