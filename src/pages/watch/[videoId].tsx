import axios from 'axios';
import type { GetServerSideProps, NextPage } from 'next';
import Head from 'next/head';
import React from 'react';
import { Video } from 'src/components/atoms/Video';
import { Header } from 'src/components/molecules/Header';
import { VideoInfo } from 'src/components/organisms/VideoInfo';
import { Container } from 'src/components/templates/Container';
import { VideoType } from 'src/Types/VideoType';

export type VideoPageProps = {
  video: VideoType;
};

const VideoPage: NextPage<VideoPageProps> = ({ video }) => {
  return (
    <>
      <Head>
        <title>{video.title}</title>
      </Head>
      <Header />
      <Container>
        <Video link={video.src} preview={false} removePoster />
        <VideoInfo video={video} />
      </Container>
    </>
  );
};

export default VideoPage;

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  if (!params || !params.videoId) {
    throw { status: 'error' };
  }
  const response = await axios.get(
    `http://${process.env.API_URL}/api/video/${params.videoId}`
  );

  const video = response.data;

  return { props: { video } };
};
