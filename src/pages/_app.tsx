import type { AppProps } from 'next/app';
import 'src/scss/index.scss';
import { Provider } from 'react-redux';
import { store } from 'src/core/store';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;
