import axios from 'axios';
import type { NextPage } from 'next';
import Head from 'next/head';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Header } from 'src/components/molecules/Header';
import { NoVideo } from 'src/components/molecules/NoVideo';
import { FavoriteBlock } from 'src/components/organisms/FavoriteBlock';
import { Container } from 'src/components/templates/Container';
import { IState } from 'src/core/store';
import { VideoType } from 'src/Types/VideoType';

export type FavoriteProps = {
  videos: VideoType[];
};

const Playlist: NextPage<FavoriteProps> = ({ videos }) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const currentVideos = useSelector((state: IState) => state.videoList);

  const setNextIndex = () => {
    if (currentIndex < currentVideos.length - 1) {
      setCurrentIndex(currentIndex + 1);
    }
  };

  return (
    <>
      <Head>
        <title>Playlist</title>
      </Head>
      <Header />
      <Container>
        {currentVideos.length !== 0 ? (
          <FavoriteBlock
            index={currentIndex}
            videos={currentVideos}
            setVideo={setCurrentIndex}
            onEnd={setNextIndex}
          />
        ) : (
          <NoVideo text="There is no video added to playlist :(" />
        )}
      </Container>
    </>
  );
};

export default Playlist;

export const getServerSideProps = async () => {
  const response = await axios.get(`http://${process.env.API_URL}/api/video`);
  const videos = response.data.content;

  return { props: { videos } };
};
