export type VideoType = {
  id: number;
  title: string;
  src: string;
  tagId: number[];
};
