// import { AnyAction, createStore } from '@reduxjs/toolkit';

import { AnyAction, createStore } from 'redux';
import { VideoType } from 'src/Types/VideoType';

const TEST_VIDEOS_FAVORITE = [
  {
    id: 1,
    title: 'Rainy road',
    src: 'https://assets.mixkit.co/videos/preview/mixkit-going-down-a-curved-highway-down-a-mountain-41576-large.mp4',
    tagId: [1, 2, 3],
  },
  {
    id: 2,
    title: 'Man and woman talking',
    src: 'https://assets.mixkit.co/videos/preview/mixkit-two-coworkers-talking-and-laughing-4872-large.mp4',
    tagId: [1, 2, 3],
  },
];

export interface IState {
  favoriteList: VideoType[];
  videoList: VideoType[];
}

const defaultState: IState = {
  favoriteList: [],
  videoList: [],
};

const reducer = (state = defaultState, action: AnyAction) => {
  switch (action.type) {
    case 'video/add': {
      state.videoList.push(action.payload);
      return state;
    }
    case 'video/remove': {
      const index = state
        .videoList!.map((video) => video.id)
        .indexOf(action.payload);

      if (index !== -1 && state.videoList) {
        state.videoList.splice(index, 1);
      }

      return state;
    }
    case 'video/set': {
      return { ...state, videoList: action.payload };
    }
    case 'favorite/add': {
      state.favoriteList.push(action.payload);

      return state;
    }
    case 'favorite/remove': {
      const index = state
        .favoriteList!.map((video) => video.id)
        .indexOf(action.payload);

      if (index !== -1 && state.favoriteList) {
        state.favoriteList.splice(index, 1);
      }

      return state;
    }
    case 'favorite/set': {
      return { ...state, favoriteList: action.payload };
    }
    default: {
      return state;
    }
  }
};

export const store = createStore(reducer);
